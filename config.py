from pathlib import Path

# Définition du répertoire de base
BASE_DIR = Path(__file__).resolve().parent.parent

# Chemins vers les fichiers de données spécifiques
BODY_DATA_PATH = BASE_DIR / 'body.data.txt'
BODY_DESCRIPTION_PATH = BASE_DIR / 'body.txt'

# Chemin vers le répertoire de sortie
OUTPUT_DIR = BASE_DIR / 'outputs'
if not OUTPUT_DIR.exists():
    OUTPUT_DIR.mkdir()